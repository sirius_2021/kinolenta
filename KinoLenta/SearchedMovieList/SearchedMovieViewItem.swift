//
//  SearchedmovieViewItem.swift
//  KinoLenta
//
//  Created by Dmitry Trifonov on 10.12.2021.
//

import UIKit

struct SearchedMovieViewItem {
    let id: Int
    let imageURL: URL?
    let title: String
    let genre: String?
    let overview: String?
    let rating: Double?
}
